$('.nw-dropdown').click(function () {
        $(this).attr('tabindex', 1).focus();
        $(this).toggleClass('active');
        $(this).find('.nw-dd-options').slideToggle(300);
    });
    $('.nw-dropdown').focusout(function () {
        $(this).removeClass('active');
        $(this).find('.nw-dd-options').slideUp(300);
    });
    $('.nw-dropdown .nw-dd-options span').click(function () {
        $(this).parents('.nw-dropdown').find('#nw-dd-fist-value').text($(this).text());
        $(this).parents('.nw-dropdown').find('input').attr('value', $(this).attr('id'));
    });
/*End Select Box js*/


