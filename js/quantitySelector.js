(function() {
  $(function() {
    return $(".nw-btn-sel-substract,.nw-btn-sel-add").click(function(e) {
      var inc_dec, qty, result, selectBtn;
      inc_dec = $(this).hasClass("nw-btn-sel-substract") ? -1 : 1;
      qty = $("[name=quantity]");
      selectBtn = $(".nw-btn-qty-select");
      result = parseInt(qty.val()) + inc_dec;
    console.log(result);
      if (result <= 0) {
      	selectBtn.show();
      	return qty.val(0)
      }
      return qty.val(result);
    });
  });

  $(function() {
    return $(".nw-btn-qty-select").click(function(e) {
    	var qty;
    	$(this).hide();
    	qty = $("[name=quantity]");
		return qty.val(1)
		
    });
  });

}).call(this);
